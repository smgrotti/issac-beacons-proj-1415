package it.isice.issac.pervasiveblelib.core;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.isice.issac.pervasiveblelib.event.BeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEventListener;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.interfaces.IPositioningListener;
import it.isice.issac.pervasiveblelib.utils.BeaconId;
import it.isice.issac.pervasiveblelib.utils.BeaconMeasure;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Concrete class for a positioning manager: user have to set the ID of the beacons
 * to which it is interested in, and the manager notify its listener with a
 * {@link it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent} containing
 * all the measures of proximity for them.
 *
 * @see it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent
 * @see AbstractManager
 */
public class PositioningManager extends AbstractManager{

    protected volatile ArrayList<String> interestingBeacons;

    public PositioningManager(BeaconManager beaconManager){
        super(beaconManager);
        interestingBeacons = new ArrayList<>();
    }

    public PositioningManager(BeaconManager beaconManager, ArrayList<String> interestingBeacons){
        super(beaconManager);
        this.interestingBeacons = interestingBeacons;
    }

    /**
     * Notify all the listeners registered to this manager with the event containing the
     * the measure relative to every beacon actually discovered in the ranging process.
     *
     * @param event the event with which notify the listeners
     */
    @Override
    public void notifyListeners(IBeaconEvent event) {
        if (event instanceof IBeaconPositioningEvent) {
            for (IBeaconEventListener l : listeners) {
                log("A LISTENER");
                if(l instanceof IPositioningListener){
                    ((IPositioningListener)l).onPositionChanged((IBeaconPositioningEvent)event);
                }
            }
        }
    }

    /**
     * This method implements the manager logic in updating the listeners: in this case each listener
     * is notified for the nearest beacon (aka resource) found in the ranging process.
     * */

    /**
     * This method implements the manager logic to be executed when beacons are discovered:
     * a new event of type {@link it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent}
     * is created containing info about every beacon discovered in the last ranging step that is
     * declared to be interesting and then the listener are updated by means of the notifyListener
     * method.
     *
     * @param region region observed
     * @param list list of beacons discovered
     */
    @Override
    protected synchronized void discoveredBeacons(Region region, List<Beacon> list) {
        BeaconPositioningEvent event = null;
        //TODO create the event coherently by aggregating the resource
        //TODO PUT HERE THE CODE TO DEDUCE THE POSTION FROM THE ZONES
        HashMap<String, IBeaconMeasure> measures = new HashMap<>();
        for(Beacon b:list){
            BeaconId bId = new BeaconId(b.getMajor(),b.getMinor());
            UtilsKB.log("Beacon "+bId.getId()+" discovered with prox = "+Utils.computeProximity(b));
            for(String beacon : interestingBeacons) {
                if(bId.getId().equals(beacon)){
                    //trovato un beacon interessante, aggiungi alla map
                    measures.put(bId.getId(), new BeaconMeasure(bId.getId(),
                            Utils.computeAccuracy(b),Utils.computeProximity(b)));
                }
            }
        }
        event = new BeaconPositioningEvent(measures);
        notifyListeners(event);
    }
}
