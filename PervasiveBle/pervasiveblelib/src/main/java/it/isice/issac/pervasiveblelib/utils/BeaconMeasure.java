package it.isice.issac.pervasiveblelib.utils;

import com.estimote.sdk.Utils;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;

/**
 * Concrete class for a measure of proximity and distance for a beacon.
 */
public class BeaconMeasure implements IBeaconMeasure{
    protected String beaconId;
    protected double distance;
    protected Utils.Proximity proximity;

    public BeaconMeasure(String beaconId, double distance, Utils.Proximity proximity) {
        this.beaconId = beaconId;
        this.distance = distance;
        this.proximity = proximity;
    }

    @Override
    public String getBeaconId() {
        return beaconId;
    }

    @Override
    public Utils.Proximity getProximity() {
        return proximity;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "BeaconId->"+beaconId+"--"+
                "D=> "+distance+"--"+
                "Proximity => "+proximity.toString()+"\n";
    }
}
