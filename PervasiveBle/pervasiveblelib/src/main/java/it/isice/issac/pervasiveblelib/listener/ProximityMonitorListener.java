package it.isice.issac.pervasiveblelib.listener;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Monitor listeners that encapsulates the last value retrieved about the closest beacon and
 * lets the user decide when he wants to get the info by calling getCurMeasure() method:
 * this class is implemented like a Monitor to handle concurrency issues (read and write problem).
 */
public class ProximityMonitorListener extends AbstractProximityListener {

    private volatile IBeaconMeasure curMeasure;

    /**
     * Overridden method to be synchronized as this class has to be a monitor.
     * @param ev
     */
    @Override
    public synchronized void onPositionChanged(IBeaconEvent ev) {
        super.onPositionChanged(ev);
    }

    @Override
    public synchronized void onPositionChanged(IBeaconProximityEvent ev) {
        UtilsKB.log("Proximity event arrived in monitor");
        curMeasure = ev.getClosestMeasure();
    }

    /**
     * Method for retrieving last data collected about closest beacon.
     * @return the measure relative to the last closest beacon
     */
    public synchronized IBeaconMeasure getCurMeasure(){
        return curMeasure;
    }
}
