package it.isice.issac.pervasiveblelib.interfaces;

/**
 * Interface for a manager, the central entity in the architecture of this library: referencing to
 * the Observer pattern, this entity has the role of Observable, notifying its previous registered
 * observer entities when beacons are discovered in the ranging process.
 */
public interface ICoreManager {
    void addListener(IBeaconEventListener l);

    void removeListener(IBeaconEventListener l);

    void notifyListeners(IBeaconEvent event);

    void addBeacon(String beaconId);

    void removeBeacon(String beaconId);

    void start();

    void stop();

    void destroy();
}
