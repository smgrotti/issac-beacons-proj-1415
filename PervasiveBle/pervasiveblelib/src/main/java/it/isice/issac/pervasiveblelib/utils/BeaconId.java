package it.isice.issac.pervasiveblelib.utils;

import java.util.UUID;

/**
 * Object reepresenting a beacon unique ID made of Proximity UUID, major and minor number.
 * Often only major and minor number are used to uniquely identify beacons a certain region
 * identified by a proximity UUID.
 */
public class BeaconId {

    protected int major;
    protected int minor;

    public BeaconId(int major, int minor/*, UUID proximityUUID*/) {
        this.major = major;
        this.minor = minor;
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }


    /**
     * The most useful method as it returns the id in the string form.
     * @return the id of the beacon in string form
     */
    public String getId(){
       return major+":"+minor;
    }

    @Override
    public String toString() {
        return "BeaconId{" +
                ", major=" + major +
                ", minor=" + minor +
                '}';
    }
}
