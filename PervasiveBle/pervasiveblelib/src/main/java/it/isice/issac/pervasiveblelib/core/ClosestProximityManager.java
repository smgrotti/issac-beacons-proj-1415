package it.isice.issac.pervasiveblelib.core;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.List;

import it.isice.issac.pervasiveblelib.event.BeaconProximityEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEventListener;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;
import it.isice.issac.pervasiveblelib.interfaces.IProximityListener;
import it.isice.issac.pervasiveblelib.utils.BeaconId;
import it.isice.issac.pervasiveblelib.utils.BeaconMeasure;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Concrete class for a manager that detects proximity to the closest beacon and notify that
 * to the previously registered listeners. It should be noted that this manager has to be used
 * in couple with listeners capable of exploiting {@link it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent}
 *
 * @see it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent
 */
public class ClosestProximityManager extends AbstractManager{

    public ClosestProximityManager(BeaconManager beaconManager){
        super(beaconManager);
    }

    /**
     * Notify all the listeners registered to this manager with the event containing the
     * closest beacon and its proximity level.
     *
     * @param event the event with which notify the listeners
     */
    @Override
    public void notifyListeners(IBeaconEvent event) {
        if(event instanceof IBeaconProximityEvent) {
            for (IBeaconEventListener l : listeners) {
                log("A LISTENER");
                if(l instanceof IProximityListener){
                    ((IProximityListener)l).onPositionChanged((IBeaconProximityEvent)event);
                }
            }
        }
    }

    /**
     * This method implements the manager logic to be executed when beacons are discovered:
     * a new event of type {@link it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent}
     * is created containing info about the closest beacon and then the listener are updated by means
     * notifyListener method
     *
     * @param region region observed
     * @param list list of beacons discovered
     */
    @Override
    protected void discoveredBeacons(Region region, List<Beacon> list) {
        BeaconProximityEvent event = null;
        //TODO create the event coherently by aggregating the resource
        Beacon nearestBeacon = null;
        for(Beacon b : list){
            UtilsKB.log("Discovered Beacon " + b.getMajor()+":"+b.getMinor());
            if(nearestBeacon != null){
                if(Utils.computeAccuracy(b) < Utils.computeAccuracy(nearestBeacon)){
                    nearestBeacon = b;
                }
            } else {
                nearestBeacon = b;
            }
        }
        if(nearestBeacon != null){
            String bId = new BeaconId(nearestBeacon.getMajor(),nearestBeacon.getMinor()).getId();
            BeaconMeasure m = new BeaconMeasure(bId, Utils.computeAccuracy(nearestBeacon),Utils.computeProximity(nearestBeacon));
            event = new BeaconProximityEvent(m);

            notifyListeners(event);
        } else {
            //no interesting beacon found, void special event
            event = new BeaconProximityEvent(new BeaconMeasure(null,UtilsKB.INFINITY, Utils.Proximity.UNKNOWN));

            notifyListeners(event);
        }
    }

    @Override
    public void addBeacon(String beaconId) {

    }

    @Override
    public void removeBeacon(String beaconId) {

    }
}
