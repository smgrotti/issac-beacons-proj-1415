package it.isice.issac.pervasiveblelib.event;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;

/**
 * Concrete class for an event that encapsulated info about the closest beacon discovered in the
 * ranging process in the form of a IBeaconMeasure
 *
 * @see IBeaconMeasure
 */
public class BeaconProximityEvent implements IBeaconProximityEvent{
    protected IBeaconMeasure closestMeasure;

    public BeaconProximityEvent(IBeaconMeasure closestMeasure) {
        this.closestMeasure = closestMeasure;
    }


    @Override
    public IBeaconMeasure getClosestMeasure() {
        return closestMeasure;
    }

    /**
     *
     * @return default rep of the ev
     */
    @Override
    public String getDefaultRep() {
        return
                this.getClass().getSimpleName()+
                "("+closestMeasure.getBeaconId()
                +","+closestMeasure.getProximity()
                +","+closestMeasure.getDistance()+")";
    }
}
