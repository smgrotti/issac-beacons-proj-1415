package it.isice.issac.pervasiveblelib.listener;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.interfaces.IPositioningListener;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Abstract class for a positioning listener.
 */
public abstract class AbstractPositioningListener implements IPositioningListener{

    protected IBeaconPositioningEvent currentEv;

    /**
     * If this method is called with an event that is of the subtype IBeaconPositioningEvent
     * the overloaded abstract method is called (Template method).
     * @param ev
     */
    @Override
    public void onPositionChanged(IBeaconEvent ev) {
        UtilsKB.log("ABS-POS-LIST onChanged called");
        if(ev instanceof IBeaconPositioningEvent){
            currentEv = (IBeaconPositioningEvent) ev;
            onPositionChanged((IBeaconPositioningEvent) ev);
            return;
        }
    }

    public abstract void onPositionChanged(IBeaconPositioningEvent ev);
}
