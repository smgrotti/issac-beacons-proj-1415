package it.isice.issac.pervasiveblelib.core;

import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.isice.issac.pervasiveblelib.event.BeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.event.BeaconProximityEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEventListener;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;
import it.isice.issac.pervasiveblelib.interfaces.IPositioningListener;
import it.isice.issac.pervasiveblelib.interfaces.IProximityListener;
import it.isice.issac.pervasiveblelib.utils.BeaconId;
import it.isice.issac.pervasiveblelib.utils.BeaconMeasure;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Concrete class for a manager that can manage both type of events, about proximity to the closest beacon
 * {@link it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent}
 * and positioning with respect to the beacons discovered
 * {@link it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent}.
 * So this manager can handle both type of listener and updates them with the appropriate
 * event, discriminating by the type of the listener using java reflection.
 *
 * @see it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent
 * @see it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent
 */
public class EventManager extends AbstractManager {

    protected ArrayList<IProximityListener> proximityListeners;
    protected ArrayList<IPositioningListener> positioningListeners;

    public EventManager(BeaconManager beaconManager) {
        super(beaconManager);
        proximityListeners = new ArrayList<>();
        positioningListeners = new ArrayList<>();
    }


    public EventManager(BeaconManager beaconManager, ArrayList<String> interestingBeacons) {
        //super(beaconManager);
        this(beaconManager);
        for(String s: interestingBeacons){
            this.interestingBeacons.add(s);
        }
    }

    /**
     * Override parent method as the listeners are handled in different collections
     * based on their type. This method add the listener to the right collection basing its decision
     * on the type of the listener retrieved thorugh java reflection.
     *
     * @param l the listener to be added
     */
    @Override
    public void addListener(IBeaconEventListener l) {
        if(l instanceof IProximityListener){
            log("ADDED A PROXIMITY LISTENER");
            proximityListeners.add((IProximityListener)l);
        } else if(l instanceof IPositioningListener){
            log("ADDED A POSITIONING LISTENER");
            positioningListeners.add((IPositioningListener)l);
        } else {
            log("ADDED A GENERIC LISTENER");
            listeners.add(l);
        }
    }

    /**
     * Override parent method as the listeners are handled in different collections
     * based on their type. This method remove the listener in the right collection basing its decision
     * on the type of the listener retrieved thorugh java reflection.
     *
     * @param l the listener to be removed
     */
    @Override
    public void removeListener(IBeaconEventListener l) {
        if(l instanceof IProximityListener){
            log("REMOVE A PROXIMITY LISTENER");
            proximityListeners.remove(l);
        } else if(l instanceof IPositioningListener){
            log("REMOVE A POSITIONING LISTENER");
            positioningListeners.remove(l);
        } else {
            log("REMOVE A GENERIC LISTENER");
            listeners.remove(l);
        }
    }

    /**
     * Notify the listener with an instance of the specialized event appropriate for the
     * soecific kind of listener: for example a IBeaconProximityEvent for a IProximityListener and
     * a IBeaconPositioningEvent for a IPositioningListener.
     *
     * @param event the event with which notify the listeners
     */
    @Override
    public void notifyListeners(IBeaconEvent event) {
        log("NOTIFYING LISTENERS.....");
        if(event instanceof IBeaconProximityEvent){
            for(IProximityListener l : proximityListeners){
                log("A PROXLISTENER");
                l.onPositionChanged((IBeaconProximityEvent) event);
            }
        } else if(event instanceof IBeaconPositioningEvent){
            for(IPositioningListener l : positioningListeners){
                log("A POSLISTENER");
                l.onPositionChanged((IBeaconPositioningEvent) event);
            }
        } else {
            for(IBeaconEventListener l : listeners){
                log("A LISTENER");
                l.onPositionChanged(event);
            }
        }
    }

    /**
     * When beacons are discovered a IBeaconProximityEvent and a IBeaconPositioningEvent are created
     * (if the beacons discovered are part of the interesting ones) and then are used to notify the
     * listeners previously registered.
     *
     * @param region region observed
     * @param list list of beacons discovered
     */
    @Override
    protected void discoveredBeacons(Region region, List<Beacon> list) {
        BeaconPositioningEvent posEvent = null;
        BeaconProximityEvent proxEvent = null;

        HashMap<String, IBeaconMeasure> measures = new HashMap<>();
        Beacon nearestBeacon = null;

        for(Beacon b:list){
            BeaconId bId = new BeaconId(b.getMajor(),b.getMinor());
            log("Beacon " + bId.getId() + " discovered with prox = " + Utils.computeProximity(b));
            for(String beacon : interestingBeacons) {
                if(bId.getId().equals(beacon)){
                    //trovato un beacon interessante, aggiungi alla map
                    measures.put(bId.getId(), new BeaconMeasure(bId.getId(),
                            Utils.computeAccuracy(b),Utils.computeProximity(b)));
                    //poi guarda se e most close
                    if(nearestBeacon != null){
                        if(Utils.computeAccuracy(b) < Utils.computeAccuracy(nearestBeacon)){
                            nearestBeacon = b;
                        }
                    } else {
                        nearestBeacon = b;
                    }
                }
            }
        }
        posEvent = new BeaconPositioningEvent(measures);

        if(nearestBeacon != null){
            String closestBId = new BeaconId(nearestBeacon.getMajor(),nearestBeacon.getMinor()).getId();
            BeaconMeasure m = new BeaconMeasure(closestBId, Utils.computeAccuracy(nearestBeacon),Utils.computeProximity(nearestBeacon));
            proxEvent = new BeaconProximityEvent(m);
        } else {
            //no interesting beacon found, void special event
            proxEvent = new BeaconProximityEvent(new BeaconMeasure(null,UtilsKB.INFINITY, Utils.Proximity.UNKNOWN));
        }

        //notify proximity
        notifyListeners(proxEvent);
        notifyListeners(posEvent);
    }

    @Override
    protected void log(String msg){
        Log.d(UtilsKB.LIB_TAG, "[EVENTMANAGER] :: " + msg);
    }
}
