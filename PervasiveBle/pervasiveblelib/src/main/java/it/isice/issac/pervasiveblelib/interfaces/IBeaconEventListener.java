package it.isice.issac.pervasiveblelib.interfaces;

/**
 * General interface for a listener for a manager class: it is notified whenever the manager
 * has discovered beacons in the ranging cicle.
 */
public interface IBeaconEventListener {
    void onPositionChanged(IBeaconEvent ev);
}
