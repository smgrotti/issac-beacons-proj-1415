package it.isice.issac.pervasiveblelib.utils;

import java.util.HashMap;

public class RoomConfigurator {
    protected HashMap<Cardinal,String> cardinalBeacons = new HashMap<Cardinal,String>();

    public RoomConfigurator(String upBeacon, String rightBeacon, String downBeacon, String leftBeacon){
        cardinalBeacons.put(Cardinal.UP,upBeacon);
        cardinalBeacons.put(Cardinal.RIGHT,rightBeacon);
        cardinalBeacons.put(Cardinal.DOWN,downBeacon);
        cardinalBeacons.put(Cardinal.LEFT,leftBeacon);
    }

    public String getCardinalBeacon(Cardinal c){
        return cardinalBeacons.get(c);
    }

    public void setCardinalBeacon(Cardinal c, String b){
        cardinalBeacons.put(c,b);
    }

    public String[] getCardinalBeaconArray(){
        return new String[]{
                cardinalBeacons.get(Cardinal.UP),
                cardinalBeacons.get(Cardinal.RIGHT),
                cardinalBeacons.get(Cardinal.DOWN),
                cardinalBeacons.get(Cardinal.LEFT)};
    }
}
