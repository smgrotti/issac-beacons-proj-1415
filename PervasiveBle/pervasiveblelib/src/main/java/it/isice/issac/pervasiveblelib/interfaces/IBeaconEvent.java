package it.isice.issac.pervasiveblelib.interfaces;

/**
 * Interface for a general event relative to beacon, that a core manager can notify to its listeners.
 */
public interface IBeaconEvent {
    public String getDefaultRep();
}
