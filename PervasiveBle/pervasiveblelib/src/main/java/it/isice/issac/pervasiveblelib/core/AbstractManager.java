package it.isice.issac.pervasiveblelib.core;

import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconEventListener;
import it.isice.issac.pervasiveblelib.interfaces.ICoreManager;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Core class of the framework that represents an abstract manager:it provides an interface to
 * Estimote SDK, as a BeaconManager is required, and notify each registered listener whenone or more
 * of the beacons declared 'interesting' are discovered in the ranging process.
 * This class has the role of Observable in the OO Observer pattern, and the logic with which
 * the listeners are notified is left to the concrete manager that will subclass this class, as the
 * ralative method is declared abstract, like the logic associated to the new discovery of beacons
 * in the ranging process actuated by the BLE sensor.
 *
 */
public abstract class AbstractManager implements ICoreManager{

    protected BeaconManager beaconManager;
    protected Region region;

    protected ArrayList<IBeaconEventListener> listeners;

    protected volatile ArrayList<String> interestingBeacons;

    public AbstractManager(BeaconManager beaconManager){
        //set the manager and connect it, it will be started later!
        this.beaconManager = beaconManager;
        //create a region for all the beacons you are interested in (or one in particular if you add maj and min)
        region = new Region("ranged region", UUID.fromString(UtilsKB.REGION_UUID), null, null);

        listeners = new ArrayList<IBeaconEventListener>();
        interestingBeacons = new ArrayList<>();

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                discoveredBeacons(region, list);
            }
        });

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                serviceReady();
            }
        });
        log("DONE CREATING");
    }

    /**
     * Add (register) a listener interested in being notified when beacons are discovered.
     * @param l the listener to be added
     */
    public void addListener(IBeaconEventListener l) {
        log("ADDED A LISTENER");
        listeners.add(l);
    }

    /**
     * Deregister a listener
     * @param l the listener to be removed
     */
    public void removeListener(IBeaconEventListener l) {
        listeners.remove(l);
    }

    /**
     * Abstract method left to the concrete subclasses, template method pattern.
     * It should be implemented with the logic with which notify the listeners.
     * @param event the event with which notify the listeners
     */
    public abstract void notifyListeners(IBeaconEvent event);

    /**
     * Start the ranging process by starting Estimote beacon manager.
     */
    public void start(){
        beaconManager.startRanging(region);
        log("beacon-manager start ranging");
    }

    /**
     * Stop the ranging process by stoppin Estimote beacon manager.
     */
    public void stop(){
        beaconManager.stopRanging(region);
        log("beacon-manager stop ranging");
    }

    /**
     * Destroy the manager by disconnecting Estimote beacon manager, to be called only
     * when the app is closed.
     */
    public void destroy(){
        beaconManager.disconnect();
        log("beacon-manager disconnected");
    }

    /**
     * Abstract method that has to be implemented in subclasses, in which the programmer
     * can specify what to do when beacons are discovered: typically the listener will be notified
     * inside this method by calling notifyListeners().
     * @param region region observed
     * @param list list of beacons discovered
     */
    protected abstract void discoveredBeacons(Region region, List<Beacon> list);

    /**
     * Called when Estimote Beacon Manager has been connected and so the ranging process is ready
     * and can be started
     */
    protected void serviceReady(){
        beaconManager.startRanging(region);
        log("beacon-manager start ranging when service is connected");
    }

    /**
     * Add a beacon to the interesting ones, the id is a string in the form "major:minor",
     * see BeaconId class: you can create a BeaconId by passing to it major and minor number and
     * then retrieve the id from that object.
     *
     * @see it.isice.issac.pervasiveblelib.utils.BeaconId
     * @param beaconId the id of the beacon to be added
     */
    public synchronized void addBeacon(String beaconId){
        interestingBeacons.add(beaconId);
    }

    /**
     * Remove a beacon from the interesting ones, the id is a string in the form "major:minor",
     * see BeaconId class: you can create a BeaconId by passing to it major and minor number and
     * then retrieve the id from that object.
     * @param beaconId the id of the beacon to be removed
     */
    public synchronized void removeBeacon(String beaconId){
        for(String b : interestingBeacons){
            if(b.equals(beaconId)){
                interestingBeacons.remove(b);
                //break;
            }
        }
    }

    /**
     * Log utility
     * @param msg msg to be logged
     */
    protected void log(String msg){
        Log.d(UtilsKB.LIB_TAG, "[ABSTRACTMANAGER] :: " + msg);
    }
}
