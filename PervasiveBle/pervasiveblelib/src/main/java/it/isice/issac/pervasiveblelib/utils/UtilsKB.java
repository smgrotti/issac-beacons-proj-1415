package it.isice.issac.pervasiveblelib.utils;

import android.util.Log;

/**
 * Utils constant class.
 */
public final class UtilsKB {
    public static final String LIB_TAG = "BEACONS-ESTIMOTE-LIB";
    public static final String REGION_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    public static final double INFINITY = 1000000000;
    public static void log(String msg){
        Log.d(UtilsKB.LIB_TAG,msg);
    }
}
