package it.isice.issac.pervasiveblelib.interfaces;

/**
 * Interface for a positioning listener.
 */
public interface IPositioningListener extends IBeaconEventListener {
    void onPositionChanged(IBeaconPositioningEvent ev);
}
