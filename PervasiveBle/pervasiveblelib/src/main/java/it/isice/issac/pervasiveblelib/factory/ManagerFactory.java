package it.isice.issac.pervasiveblelib.factory;

import android.content.Context;

import com.estimote.sdk.BeaconManager;

import java.util.ArrayList;

import it.isice.issac.pervasiveblelib.core.ClosestProximityManager;
import it.isice.issac.pervasiveblelib.core.EventManager;
import it.isice.issac.pervasiveblelib.core.PositioningManager;
import it.isice.issac.pervasiveblelib.interfaces.ICoreManager;

/**
 * Factory for managers.
 */
public class ManagerFactory {

    public static final String PROXIMITY_MANAGER = "proxMan";
    public static final String POSITIONING_MANAGER = "posMan";
    public static final String GENERAL_MANAGER = "genMan";

    private static ManagerFactory factoryInstance;

    private ICoreManager manager;

    private ManagerFactory(){

    }

    /**
     * Singleton pattern, return the instance of the factory.
     * @return instance of the factory
     */
    public synchronized static ManagerFactory getFactoryInstance(){
        if(factoryInstance == null){
            factoryInstance = new ManagerFactory();
        }
        return factoryInstance;
    }

    /**
     * Return the instanc of the manager if it is not already created, else the previously created manager.
     *
     * @param type string type of the manager, see constants of this class
     * @param interestingBeacons list of  interesting beacons
     * @return instance of the manager for the framework
     */
    public synchronized ICoreManager getManager(String type, Context c, ArrayList<String> interestingBeacons){
        if(manager == null) {
            BeaconManager bMan = new BeaconManager(c);
            if (PROXIMITY_MANAGER.equals(type)) {
                manager = new ClosestProximityManager(bMan);
            } else if (POSITIONING_MANAGER.equals(type)) {
                manager = new PositioningManager(bMan,interestingBeacons);
            } else if(GENERAL_MANAGER.equals(type)){
                manager = new EventManager(bMan,interestingBeacons);
            }
        }
        return manager;
    }

    public synchronized void destroyManager(){
        manager = null;
    }
}
