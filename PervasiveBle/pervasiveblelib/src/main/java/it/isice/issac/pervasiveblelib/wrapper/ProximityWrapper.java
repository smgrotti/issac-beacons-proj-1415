package it.isice.issac.pervasiveblelib.wrapper;

import android.content.Context;

import com.estimote.sdk.BeaconManager;

import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.ICoreManager;
import it.isice.issac.pervasiveblelib.listener.ProximityMonitorListener;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Wrapper that encapsulates the retrieving of proximity event through a manager and let the user
 * know info by a thread safe get() method.
 * Higher abstraction, programmer doesn't care care about observer pattern no more
 */
public class ProximityWrapper {

    private static ProximityWrapper wrapperInstance;
    private ICoreManager manager;
    private ProximityMonitorListener l;

    private ProximityWrapper(Context c){
        manager = ManagerFactory.getFactoryInstance().getManager(ManagerFactory.PROXIMITY_MANAGER, c, null);
        l = new ProximityMonitorListener();
        UtilsKB.log("Manager class is "+manager.getClass().getSimpleName());
        manager.addListener(l);
        UtilsKB.log("WRAPPER FINISHED CREATE");
    }

    public static ProximityWrapper getWrapperInstance(Context c){
        if(wrapperInstance == null) wrapperInstance = new ProximityWrapper(c);
        return wrapperInstance;
    }

    /**
     * NB it is synchronized inside
     * @return
     */
    public IBeaconMeasure getCurrentMeasure(){
        UtilsKB.log("WRAPPER: get called");
        return l.getCurMeasure();
    }

    public void start(){
        manager.start();
    }

    public void stop(){
        manager.stop();
    }
}
