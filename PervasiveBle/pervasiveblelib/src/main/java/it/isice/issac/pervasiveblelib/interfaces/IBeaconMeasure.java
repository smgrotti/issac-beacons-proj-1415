package it.isice.issac.pervasiveblelib.interfaces;

import com.estimote.sdk.Utils;

/**
 * Interface for a single measure relative to a particular beacon, that includes the
 * estimated proximity zone and distance.
 */
public interface IBeaconMeasure {

    String getBeaconId();
    Utils.Proximity getProximity();
    double getDistance();
}
