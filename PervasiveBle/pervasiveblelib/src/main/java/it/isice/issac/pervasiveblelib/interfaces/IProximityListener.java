package it.isice.issac.pervasiveblelib.interfaces;

/**
 * Interface for a proximity listener.
 */
public interface IProximityListener extends IBeaconEventListener {
    void onPositionChanged(IBeaconProximityEvent ev);
}
