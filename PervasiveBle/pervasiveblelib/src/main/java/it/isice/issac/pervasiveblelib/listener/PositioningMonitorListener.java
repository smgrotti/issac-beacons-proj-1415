package it.isice.issac.pervasiveblelib.listener;

import java.util.Map;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;

/**
 * Monitor listeners that encapsulates the last positioning values retrieved and
 * lets the user decide when he wants to get the info by calling getCurMap() method:
 * this class is implemented like a MOnito to handle concurrency issues (read and write problem).
 */
public class PositioningMonitorListener extends AbstractPositioningListener{
    private volatile Map<String, IBeaconMeasure> curMap;

    /**
     * Overridden method to be synchronized as this class has to be a monitor.
     * @param ev
     */
    @Override
    public synchronized void onPositionChanged(IBeaconEvent ev) {
        super.onPositionChanged(ev);
    }

    /**
     *
     * @param ev
     */
    @Override
    public synchronized void onPositionChanged(IBeaconPositioningEvent ev) {
        curMap = ev.getMeasures();
    }

    /**
     * Method for retrieving last data collected about beacon positioning.
     * @return the map of last measures
     */
    public synchronized Map<String, IBeaconMeasure> getCurMap() {
        return curMap;
    }
}
