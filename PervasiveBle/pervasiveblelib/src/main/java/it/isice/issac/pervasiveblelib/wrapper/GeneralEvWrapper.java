package it.isice.issac.pervasiveblelib.wrapper;

import android.content.Context;

import com.estimote.sdk.BeaconManager;

import java.util.Map;

import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.ICoreManager;
import it.isice.issac.pervasiveblelib.listener.PositioningMonitorListener;
import it.isice.issac.pervasiveblelib.listener.ProximityMonitorListener;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Wrapper that encapsulates the retrieving of proximity and positioning event through a manager and let the user
 * know info by a thread safe get() method.
 * Higher abstraction, programmer doesn't care care about observer pattern no more
 */
public class GeneralEvWrapper {

    private static GeneralEvWrapper wrapperInstance;
    private ICoreManager manager;
    private ProximityMonitorListener proxL;
    private PositioningMonitorListener posL;

    private GeneralEvWrapper(Context c){
        manager = ManagerFactory.getFactoryInstance().getManager(ManagerFactory.GENERAL_MANAGER, c, null);
        proxL = new ProximityMonitorListener();
        posL = new PositioningMonitorListener();
        UtilsKB.log("Manager class is " + manager.getClass().getSimpleName());
        manager.addListener(proxL);
        manager.addListener(posL);
        UtilsKB.log("WRAPPER FINISHED CREATE");
    }

    public static GeneralEvWrapper getWrapperInstance(Context c){
        if(wrapperInstance == null) wrapperInstance = new GeneralEvWrapper(c);
        return wrapperInstance;
    }

    /**
     * NB it is synchronized inside
     * @return
     */
    public IBeaconMeasure getCurrentProxMeasure(){
        UtilsKB.log("WRAPPER: get called");
        return proxL.getCurMeasure();
    }

    public Map<String, IBeaconMeasure> getCurrentPosMeasure(){
        UtilsKB.log("WRAPPER: get called");
        return posL.getCurMap();
    }


    public void start(){
        manager.start();
    }

    public void stop(){
        manager.stop();
    }
}
