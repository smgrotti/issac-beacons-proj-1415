package it.isice.issac.pervasiveblelib.listener;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;
import it.isice.issac.pervasiveblelib.interfaces.IProximityListener;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Abstract class for a proximity listener -> template method.
 */
public abstract class AbstractProximityListener implements IProximityListener{
    protected IBeaconProximityEvent currentEv;

    /**
     * If this method is called with an event that is of the subtype IBeaconProximityEvent
     * the overloaded abstract method is called (Template method).
     * @param ev
     */
    @Override
    public void onPositionChanged(IBeaconEvent ev) {
        UtilsKB.log("ABS-PROX-LIST onChanged called");
        if(ev instanceof IBeaconProximityEvent){
            currentEv = (IBeaconProximityEvent) ev;
            onPositionChanged((IBeaconProximityEvent) ev);
            return;
        }
    }


    public abstract void onPositionChanged(IBeaconProximityEvent ev);
}
