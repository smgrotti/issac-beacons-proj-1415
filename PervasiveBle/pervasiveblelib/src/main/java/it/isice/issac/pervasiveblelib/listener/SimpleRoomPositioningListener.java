package it.isice.issac.pervasiveblelib.listener;

import android.util.Log;

import com.estimote.sdk.Utils;

import java.util.HashMap;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.utils.Cardinal;
import it.isice.issac.pervasiveblelib.utils.RoomConfigurator;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Abstract class for a positioning listener in a square room so the programmer can focus on application logic
 * by subclassing the methods.
 */
public abstract class SimpleRoomPositioningListener extends AbstractPositioningListener{

    protected RoomConfigurator configurator;

    public SimpleRoomPositioningListener(RoomConfigurator configurator) {
        this.configurator = configurator;
        String[] cardinalBeacons = configurator.getCardinalBeaconArray();
        Log.d(UtilsKB.LIB_TAG, "UP BEACON -> " + cardinalBeacons[0]);
        Log.d(UtilsKB.LIB_TAG, "RIGHT BEACON -> " + cardinalBeacons[1]);
        Log.d(UtilsKB.LIB_TAG, "DOWN BEACON -> " + cardinalBeacons[2]);
        Log.d(UtilsKB.LIB_TAG, "LEFT BEACON -> " + cardinalBeacons[3]);
    }

    @Override
    public void onPositionChanged(IBeaconPositioningEvent ev) {
        UtilsKB.log("Positioning Event Received :: "+ev.getDefaultRep());
        HashMap<Cardinal,Utils.Proximity> prox = new HashMap<>();
        //here we suppose that the beacon in the configurator and in the manager are the same
        //try to get the cardinal beacons from the map!
        IBeaconMeasure m = null;
        m = ev.getMeasures().get(configurator.getCardinalBeacon(Cardinal.UP));
        if(m != null){
            prox.put(Cardinal.UP,m.getProximity());
        }
        m = ev.getMeasures().get(configurator.getCardinalBeacon(Cardinal.RIGHT));
        if(m != null){
            prox.put(Cardinal.RIGHT,m.getProximity());
        }
        m = ev.getMeasures().get(configurator.getCardinalBeacon(Cardinal.DOWN));
        if(m != null){
            prox.put(Cardinal.DOWN,m.getProximity());
        }
        m = ev.getMeasures().get(configurator.getCardinalBeacon(Cardinal.LEFT));
        if(m != null){
            prox.put(Cardinal.LEFT,m.getProximity());
        }
        /*
        String[] cardinalBeacons = configurator.getCardinalBeaconArray();
        for(BeaconMeasure m : ev.getMeasures()){
            String id = m.getBeaconMajor()+":"+m.getBeaconMinor();
            for(int i=0; i<cardinalBeacons.length;i++){
                if(id.equals(cardinalBeacons[i])){
                    proximities[i] = m.getBeaconProximity();
                    switch(i){
                        case 0://up
                            Log.d(UtilsKB.LIB_TAG,i+" trovato beacon UP con prossim -> "+ m.getBeaconProximity()+"  ID -> "+m.getBeaconMajor()+":"+m.getBeaconMinor());
                            prox.put(Cardinal.UP, m.getBeaconProximity());
                            break;
                        case 1://right
                            Log.d(UtilsKB.LIB_TAG,i+" trovato beacon RIGHT con prossim -> "+ m.getBeaconProximity()+"  ID -> "+m.getBeaconMajor()+":"+m.getBeaconMinor());
                            prox.put(Cardinal.RIGHT,m.getBeaconProximity());
                            break;
                        case 2://down
                            Log.d(UtilsKB.LIB_TAG,i+" trovato beacon DOWN con prossim -> "+ m.getBeaconProximity()+"  ID -> "+m.getBeaconMajor()+":"+m.getBeaconMinor());
                            prox.put(Cardinal.DOWN,m.getBeaconProximity());
                            break;
                        case 3://left
                            Log.d(UtilsKB.LIB_TAG,i+" trovato beacon LEFT con prossim -> "+ m.getBeaconProximity()+"  ID -> "+m.getBeaconMajor()+":"+m.getBeaconMinor());
                            prox.put(Cardinal.LEFT,m.getBeaconProximity());
                            break;
                        default:
                            break;
                    }
                }
            }
        }*/
        onNewPositioning(prox);
    }

    public abstract void onNewPositioning(HashMap<Cardinal,Utils.Proximity> proximities);
}
