package it.isice.issac.pervasiveblelib.event;

import java.util.HashMap;
import java.util.Map;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;

/**
 * Concrete implementation of an event that contains information about the measure of the beacons
 * discovered in the ranging process: these measures are kept in a map that has the id of the beacon
 * (in the string form major:minor) as the key and the measure as the value.
 *
 * @see IBeaconMeasure
 */
public class BeaconPositioningEvent implements IBeaconPositioningEvent{

    protected HashMap<String, IBeaconMeasure> measures;

    public BeaconPositioningEvent(HashMap<String, IBeaconMeasure> measures) {
        this.measures = measures;
    }

    @Override
    public Map<String, IBeaconMeasure> getMeasures() {
        return measures;
    }

    /**
     *
     * @return default rep of the ev
     */
    @Override
    public String getDefaultRep() {
        String rep = this.getClass().getSimpleName()+"(";
        for(HashMap.Entry<String, IBeaconMeasure> entry: measures.entrySet()){
            rep += "\n"+entry.getKey() + "-->" + entry.getValue().getProximity();
        }
        rep += ")";
        return rep;
    }
}
