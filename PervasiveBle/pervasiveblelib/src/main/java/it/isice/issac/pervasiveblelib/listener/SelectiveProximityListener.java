package it.isice.issac.pervasiveblelib.listener;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Abstract class for a proximity listener so the programmer can focus on application logic
 * by subclassing the method relative to the appropriate proximity level: it uses the template method
 * pattern to let the final programmer decide what to do on the basis of the proximity of the closest
 * beacon discovered.
 *
 */
public abstract class SelectiveProximityListener extends AbstractProximityListener {

    public SelectiveProximityListener() {
    }

    @Override
    public void onPositionChanged(IBeaconProximityEvent ev) {
        UtilsKB.log("Proximity Event Received :: "+ev.getDefaultRep());
        String proxBeacon = ev.getClosestMeasure().getBeaconId();
        switch(ev.getClosestMeasure().getProximity()){
            case FAR:
                beaconIsFar(proxBeacon);
                break;
            case NEAR:
                beaconIsNear(proxBeacon);
                break;
            case IMMEDIATE:
                beaconIsImmediate(proxBeacon);
                break;
            default:
                break;
        }
    }

    /**
     * Called when the closest beacon is in IMMEDIATE proximity zone (<0.5m).
     * @param beaconId
     */
    public abstract void beaconIsImmediate(final String beaconId);

    /**
     * Called when the closest beacon is in NEAR proximity zone (<3.0m).
     * @param beaconId
     */
    public abstract void beaconIsNear(final String beaconId);

    /**
     * Called when the closest beacon is in FAR proximity zone (>3.0m).
     * @param beaconId
     */
    public abstract void beaconIsFar(final String beaconId);
}
