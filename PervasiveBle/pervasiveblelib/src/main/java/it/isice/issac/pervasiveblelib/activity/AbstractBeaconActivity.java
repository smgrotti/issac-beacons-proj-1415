package it.isice.issac.pervasiveblelib.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import it.isice.issac.pervasiveblelib.interfaces.ICoreManager;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Basic class for an activity that wants to handle a manager for beacons.
 * The basic functionality  of manager handling are provided, but the creation
 * of the right manager is left to overriden onCreate method, as well as
 * the listener the programmer wants to attach to it.
 *
 */
public abstract class AbstractBeaconActivity extends AppCompatActivity {

    protected ICoreManager manager;

    /**
     * This method should be overriden by subclasses that must provide an instance
     * of manager for the manager field and attach to them some listners in order to provide
     * application logic
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remember to instantiate the manager in subclasses
        //and to attach the listener
        Log.d(UtilsKB.LIB_TAG,"onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(UtilsKB.LIB_TAG,"onStart");
    }

    /**
     * When the activity comes back to foreground, tha manager has to start.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(UtilsKB.LIB_TAG, "onResume");
        manager.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(UtilsKB.LIB_TAG, "onPause");
        manager.stop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(UtilsKB.LIB_TAG, "onRestart");
    }

    /**
     * Managing of resource: stop the manager (and the ranging) when the activity
     * is no more visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(UtilsKB.LIB_TAG, "onStop");
        manager.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(UtilsKB.LIB_TAG, "onDestroy");
    }
}
