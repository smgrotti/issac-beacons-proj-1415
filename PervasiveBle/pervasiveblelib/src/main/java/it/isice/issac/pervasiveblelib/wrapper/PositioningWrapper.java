package it.isice.issac.pervasiveblelib.wrapper;

import android.content.Context;

import com.estimote.sdk.BeaconManager;

import java.util.ArrayList;
import java.util.Map;

import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.ICoreManager;
import it.isice.issac.pervasiveblelib.listener.PositioningMonitorListener;

/**
 * Wrapper that encapsulates the retrieving of positioning event through a manager and let the user
 * know info by a thread safe get() method.
 * Higher abstraction, programmer doesn't care care about observer pattern no more
 */
public class PositioningWrapper {

    private static PositioningWrapper wrapperInstance;
    private ICoreManager manager;
    private PositioningMonitorListener l;

    private PositioningWrapper(Context c,ArrayList<String> interestingBeacons){
        manager = ManagerFactory.getFactoryInstance().getManager(ManagerFactory.POSITIONING_MANAGER, c,interestingBeacons);
        l = new PositioningMonitorListener();
        manager.addListener(l);
    }

    public static PositioningWrapper getWrapperInstance(Context c, ArrayList<String> interestingBeacons){
        if(wrapperInstance == null) wrapperInstance = new PositioningWrapper(c,interestingBeacons);
        return wrapperInstance;
    }

    /**
     * NB it is synchronized inside
     * @return
     */
    public Map<String, IBeaconMeasure> getCurrentMeasure(){
        return l.getCurMap();
    }

    public void start(){
        manager.start();
    }

    public void stop(){
        manager.stop();
    }
}
