package it.isice.issac.positioningapp;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import it.isice.issac.pervasiveblelib.activity.AbstractBeaconActivity;
import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.listener.SimpleRoomPositioningListener;
import it.isice.issac.pervasiveblelib.utils.BeaconId;
import it.isice.issac.pervasiveblelib.utils.Cardinal;
import it.isice.issac.pervasiveblelib.utils.RoomConfigurator;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

public class MainActivity extends AbstractBeaconActivity {

    /*
    * beacons
    verde chiaro:
    45356:25104
    azzurro:
    2091:14665
    blu bello:
    14472:40759
    blu brutto:
    15163:19040
    */
    protected RoomConfigurator configurator;

    private BeaconManager man;

    private SimpleRoomPositioningListener roomListener;

    private TextView upBeaconTv;
    private TextView rightBeaconTv;
    private TextView downBeaconTv;
    private TextView leftBeaconTv;
    private TextView tvDebug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        man = new BeaconManager(this);

        configurator = new RoomConfigurator(
                "45356:25104",//UP-> verdino
                "14472:40759",//RIGHT -> blubello
                "2091:14665",//DOWN -> azzurrino
                "15163:19040"//LEFT -> blubrutto
        );

        ArrayList<String> beacons = new ArrayList<>();
        beacons.add(new BeaconId(45356,25104).getId());
        beacons.add(new BeaconId(14472,40759).getId());
        beacons.add(new BeaconId(2091,14665).getId());
        beacons.add(new BeaconId(15163,19040).getId());
        //manager = PositioningManager.getManagerInstance(man);
        manager = ManagerFactory.getFactoryInstance().
                getManager(ManagerFactory.POSITIONING_MANAGER, this,beacons);


        upBeaconTv = (TextView)findViewById(R.id.tv_up_beacon_res);
        rightBeaconTv = (TextView)findViewById(R.id.tv_right_beacon_res);
        downBeaconTv = (TextView)findViewById(R.id.tv_down_beacon_res);
        leftBeaconTv = (TextView)findViewById(R.id.tv_left_beacon_res);
        tvDebug = (TextView)findViewById(R.id.tv_debug);

        roomListener = new SimpleRoomPositioningListener(configurator) {

            @Override
            public void onNewPositioning(final HashMap<Cardinal, Utils.Proximity> proximities) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvDebug.setText("Beacon rilevati -> "+proximities.size());
                        if(proximities.size() == 4){
                            upBeaconTv.setText(proximities.get(Cardinal.UP).toString());
                            rightBeaconTv.setText(proximities.get(Cardinal.RIGHT).toString());
                            downBeaconTv.setText(proximities.get(Cardinal.DOWN).toString());
                            leftBeaconTv.setText(proximities.get(Cardinal.LEFT).toString());
                        }else{
                            upBeaconTv.setText("error");
                            rightBeaconTv.setText("error");
                            downBeaconTv.setText("error");
                            leftBeaconTv.setText("error");
                        }
                    }
                });
            }
        };
        manager.addListener(roomListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // manager.destroy();
        manager.removeListener(roomListener);
        Log.d(UtilsKB.LIB_TAG, "onDestroy finished");
    }
}
