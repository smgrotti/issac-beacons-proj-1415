package it.isice.issac.proximitymonitordemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.estimote.sdk.BeaconManager;

import java.util.Timer;
import java.util.TimerTask;

import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;
import it.isice.issac.pervasiveblelib.wrapper.ProximityWrapper;

/**
 * Simple example of app that exploits the framework with a pull paradigm, instatiating a wrapper
 * object that incapsulates the logic of bluetoooth beacon data retrieving. In this example, the
 * activity controls every 5 seconds the latest measure the wrapper holds (closest beacon and its
 * proximity level) and updates the view.
 */
public class ProximityMonitorActivity extends AppCompatActivity {

    private ProximityWrapper wrapper;

    private TextView nearestTv;
    private ProgressBar bar;

    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proximity_monitor);
        //get a wrapper object
        wrapper = ProximityWrapper.getWrapperInstance(this);
        //init view elements
        nearestTv = (TextView)findViewById(R.id.tv_monitor_nearest_beacon);
        bar = (ProgressBar) findViewById(R.id.monitor_time_bar);
        bar.setMax(5);
        //timer
        timer = new Timer();
        Log.d(UtilsKB.LIB_TAG, "onCreate finished");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //start the wrapper when app is in foreground
        wrapper.start();
        //task associated to the timer
        TimerTask timerTask = new TimerTask() {

            private int count = 0;

            @Override
            public void run() {
                count++;
                bar.setProgress(count%5);
                //every 5 seconds, get the last measure from the wrapper
                if(count % 5 == 0){
                    final IBeaconMeasure m = wrapper.getCurrentMeasure();
                    if(m!= null){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                nearestTv.setText(m.getBeaconId() + " is " + m.getProximity());
                            }
                        });
                    }
                }
            }
        };
        //schedule the task every second
        timer.scheduleAtFixedRate(timerTask,0,1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stop the wrapper when in background
        wrapper.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wrapper.stop();
    }
}
