package it.isice.issac.mixedbeaconapp;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.estimote.sdk.BeaconManager;

import java.util.ArrayList;

import it.isice.issac.pervasiveblelib.activity.AbstractBeaconActivity;
import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconProximityEvent;
import it.isice.issac.pervasiveblelib.listener.AbstractPositioningListener;
import it.isice.issac.pervasiveblelib.listener.AbstractProximityListener;
import it.isice.issac.pervasiveblelib.utils.BeaconId;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Simple case study for an app that notify the user of the proximity to some beacons
 * (of declared interest): it demonstrates how to work with manager and listener of the framework.
 * This particular case show how you can register booth type of listener to a general manager,
 * capable of managing both type of events.
 * In this simple case, the results are shown to the user only by means of text views.
 */
public class MainActivity extends AbstractBeaconActivity {

        /*
    * beacons
    verde chiaro:
    45356:25104
    azzurro:
    2091:14665
    blu bello:
    14472:40759
    blu brutto:
    15163:19040
    */

    //Layout textview output
    private TextView tv_prox_event;
    private TextView tv_pos_Event;

    //listeners
    private AbstractPositioningListener posListener;
    private AbstractProximityListener proxListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //create a list of interesting beacons
        ArrayList<String> beacons = new ArrayList<>();
        beacons.add(new BeaconId(45356,25104).getId());
        beacons.add(new BeaconId(14472,40759).getId());
        beacons.add(new BeaconId(2091,14665).getId());
        beacons.add(new BeaconId(15163,19040).getId());
        //obtain a manager, in this case capable of managing both type of events
        manager = ManagerFactory.getFactoryInstance().
                getManager(ManagerFactory.GENERAL_MANAGER, this, beacons);


        tv_pos_Event = (TextView)findViewById(R.id.tv_pos_result);
        tv_prox_event = (TextView)findViewById(R.id.tv_prox_result);

        //associate the application logic to the listener by implementing the abstract method
        //and register it to the manager
        //this is a proximity listener
        proxListener = new AbstractProximityListener() {
            @Override
            public void onPositionChanged(final IBeaconProximityEvent ev) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv_prox_event.setText(ev.getDefaultRep());
                    }
                });
            }
        };
        manager.addListener(proxListener);
        //this is a positioning listener
        posListener = new AbstractPositioningListener() {
            @Override
            public void onPositionChanged(final IBeaconPositioningEvent ev) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv_pos_Event.setText(ev.getDefaultRep());
                    }
                });
            }
        };
        manager.addListener(posListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // manager.destroy();
        //remove listeners
        manager.removeListener(proxListener);
        manager.removeListener(posListener);
        Log.d(UtilsKB.LIB_TAG, "onDestroy finished");
    }
}
