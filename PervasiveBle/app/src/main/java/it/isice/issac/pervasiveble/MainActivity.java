package it.isice.issac.pervasiveble;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Utils;

import java.util.ArrayList;

import it.isice.issac.pervasiveblelib.activity.AbstractBeaconActivity;
import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.listener.SelectiveProximityListener;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

public class MainActivity extends AbstractBeaconActivity{

    //TODO ADD THE STATE TO RECOGNIZE TRANSITIONS!
    private ProximityState state = new ProximityState();

    private ArrayList<String> interestingBeacons;
    private BeaconManager man;

    private TextView nearestBeaconTv;
    private TextView furtherInfoTv;

    private SelectiveProximityListener proxListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        man = new BeaconManager(this);

        nearestBeaconTv = ((TextView)findViewById(R.id.tv_nearest_beacon));
        furtherInfoTv = ((TextView)findViewById(R.id.tv_further_info));

        //no more needed????????
        interestingBeacons = new ArrayList<>();
        interestingBeacons.add("14472:40759");
        interestingBeacons.add("15163:19040");

        //manager = ClosestProximityManager.getManagerInstance(man);
        manager = ManagerFactory.getFactoryInstance().getManager(ManagerFactory.PROXIMITY_MANAGER, this,null);

        proxListener = new SelectiveProximityListener() {

            ProximityState nextState;
            @Override
            public void beaconIsImmediate(final String beaconId) {
                nextState = new ProximityState(beaconId, Utils.Proximity.IMMEDIATE);
                if(!state.equals(nextState)){
                    state = nextState;
                    //do something!!!!
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            nearestBeaconTv.setText(beaconId+" is IMMEDIATE");
                            furtherInfoTv.setText(state+"");
                        }
                    });
                }
                //else we have to do nothing! state has not changed!
            }

            @Override
            public void beaconIsNear(final String beaconId) {
                nextState = new ProximityState(beaconId, Utils.Proximity.NEAR);
                if(!state.equals(nextState)){
                    state = nextState;
                    //do something!!!!
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            nearestBeaconTv.setText(beaconId+" is NEAR");
                            furtherInfoTv.setText(state+"");
                        }
                    });
                }
            }

            @Override
            public void beaconIsFar(final String beaconId) {
                nextState = new ProximityState(beaconId, Utils.Proximity.FAR);
                if(!state.equals(nextState)){
                    state = nextState;
                    //do something!!!!
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            nearestBeaconTv.setText(beaconId+" is FAR");
                            furtherInfoTv.setText(state+"");
                        }
                    });
                }
            }
        };

        manager.addListener(proxListener);
        Log.d(UtilsKB.LIB_TAG,"onCreate finished");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // manager.destroy();
        manager.removeListener(proxListener);
        Log.d(UtilsKB.LIB_TAG,"onDestroy finished");
    }
}
