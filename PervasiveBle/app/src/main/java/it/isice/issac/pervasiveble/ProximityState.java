package it.isice.issac.pervasiveble;

import com.estimote.sdk.Utils;

public class ProximityState {

    private String beaconId;
    private Utils.Proximity beaconProximity;

    public ProximityState() {
        beaconId = null;
        beaconProximity = null;
    }

    public ProximityState(String beaconId, Utils.Proximity beaconProximity) {
        this.beaconId = beaconId;
        this.beaconProximity = beaconProximity;
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public Utils.Proximity getBeaconProximity() {
        return beaconProximity;
    }

    public void setBeaconProximity(Utils.Proximity beaconProximity) {
        this.beaconProximity = beaconProximity;
    }

    public boolean equals(ProximityState state){
        if(state.getBeaconId().equals(beaconId) && state.getBeaconProximity() == beaconProximity){
            return true;
        }
        return false;
    }

    public String toString(){
        return beaconId+" => "+beaconProximity;
    }
}
