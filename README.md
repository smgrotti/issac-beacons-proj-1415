# Repository per il progetto di ISSAC 14-15
## Michele Braccini, Simone Costanzi, Simone Grotti

## Estimote Beacons
Questo lavoro tratta l'utilizzo dei beacon nel campo dei sistemi pervasivi e adattativi: più di preciso, grazie alla *context awareness* introdotta dallo spargimento dei beacons nell'ambiente, le applicazioni possono adattarsi alle informazioni contestuali relative all'ambiente in cui sono immerse.

###Obiettivi
Gli obiettivi principali di questo lavoro erano sostanzialmente 3:

1. Effettuare dei test sulla precisione dei beacons Estimote, per stimarne le possbiilità di utilizzo
2. Progettare un framework che supporti l'utilizzo di beacons (in applicazioni Android) e ne faciliti l'utilizzo
3. Realizzazione di una semplice demo dimostrativa delle potenzialità del framework e dell'uso dei beacon

###Contenuti
Questo repository contiene il codice sviluppato per il framework (comprensivo di piccoli esempi da considerarsi come how-to e guide al suo utilizzo) e per la demo applicativa.
Tutti i contenuti sono stati descritti in una relazione (in formato pdf), disponibile nella sezione download, insieme al file .aar per il framework.

###Contatti

* *Simone Grotti*: simone.grotti2@studio.unibo.it
* *Michele Braccini*: michele.braccini2@studio.unibo.it
* *Simone Costanzi*: simonce.costanzi@studio.unibo.it