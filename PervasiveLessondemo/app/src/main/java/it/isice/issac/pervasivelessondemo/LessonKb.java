package it.isice.issac.pervasivelessondemo;

import android.content.Context;

import java.util.Calendar;
import java.util.HashMap;

import it.isice.issac.pervasiveblelib.utils.BeaconId;

/**
 * Mock demo lesson db.
 */
public class LessonKb {

    private enum Room{ROOM_C, ROOM_D}

    private HashMap<String,Room> rooms;
    private HashMap<String,String> lessons;

    public LessonKb(Context c) {
        lessons = new HashMap<>();
        lessons.put(c.getResources().getString(R.string.aula_a_id),c.getResources().getString(R.string.issac));
        lessons.put(new BeaconId(2091, 14665).getId(),c.getResources().getString(R.string.issac));
        lessons.put(c.getResources().getString(R.string.aula_b_id), c.getResources().getString(R.string.issac));
        lessons.put(new BeaconId(45356, 25104).getId(), c.getResources().getString(R.string.issac));
        lessons.put(new BeaconId(16055, 34437).getId(), c.getResources().getString(R.string.sir));
        lessons.put(new BeaconId(58177, 52282).getId(), c.getResources().getString(R.string.sir));

        rooms = new HashMap<>();
        rooms.put(new BeaconId(45356, 25104).getId(),Room.ROOM_C);
        rooms.put(new BeaconId(2091, 14665).getId(),Room.ROOM_C);
        rooms.put(new BeaconId(14472, 40759).getId(),Room.ROOM_C);
        rooms.put(new BeaconId(15163, 19040).getId(),Room.ROOM_C);
        rooms.put(new BeaconId(16055, 34437).getId(),Room.ROOM_D);
        rooms.put(new BeaconId(58177, 52282).getId(),Room.ROOM_D);

    }

    public String getLesson(String room){
        return lessons.get(room);
    }

    public Room getRoom(String beaconId){
        return rooms.get(beaconId);
    }
}
