package it.isice.issac.pervasivelessondemo;

import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

import it.isice.issac.pervasiveblelib.activity.AbstractBeaconActivity;
import it.isice.issac.pervasiveblelib.factory.ManagerFactory;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconMeasure;
import it.isice.issac.pervasiveblelib.interfaces.IBeaconPositioningEvent;
import it.isice.issac.pervasiveblelib.listener.AbstractPositioningListener;
import it.isice.issac.pervasiveblelib.utils.BeaconId;
import it.isice.issac.pervasiveblelib.utils.UtilsKB;

/**
 * Case study demo for the framework designed for the project of ISSAC 14/15 course.
 * This is the main (and only) activity of an app that is designed to recognize when a student
 * is inside a room attending a lesson and download for him the slides for the lesson, exploiting
 * context-awareness given by the beacons. In fact, beacons are placed in rooms so that every
 * point where a student can sit down is "covered" by two beacons, thus discriminating if he is inside
 * the room.
 * This app has also the purpose of showing how the framework and the concepts it proposes can
 * speed up development process, as the programmer can focus on the application logic.
 */
public class MainActivity extends AbstractBeaconActivity implements SensorEventListener{
    //url to download mock pdf
    public String issac_pdf_url = "http://isi-tomcat.csr.unibo.it:8080/~simone.grotti2/pdf/isac-fake-slides.pdf";
    public String sir_pdf_url = "http://isi-tomcat.csr.unibo.it:8080/~simone.grotti2/pdf/isr-fake-slides.pdf";
    //accelerometer to detect if phone is laying on desk
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private ArrayList<String> interestingBeacons;
    private AbstractPositioningListener posListener;

    private TextView stateTv;
    private TextView infoTv;
    private TextView debugTv;
    private ImageView iconIv;
    //mock lesson db
    private LessonKb lessonDb;

    private boolean downloaded = false;

    private volatile float[] accValues = new float[3];

    private enum LessonState{FAR_LESSON,CLOSE_LESSON,INSIDE_LESSON};
    //state
    private LessonState state = LessonState.FAR_LESSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        ArrayList<String> interestingBeacons = new ArrayList<>();
        interestingBeacons.add(new BeaconId(45356, 25104).getId());
        interestingBeacons.add(new BeaconId(14472, 40759).getId());
        interestingBeacons.add(new BeaconId(2091, 14665).getId());
        interestingBeacons.add(new BeaconId(15163, 19040).getId());
        interestingBeacons.add(new BeaconId(16055, 34437).getId());
        interestingBeacons.add(new BeaconId(58177, 52282).getId());

        stateTv = (TextView) findViewById(R.id.tv_state);
        infoTv = (TextView) findViewById(R.id.tv_info);
        debugTv = (TextView) findViewById(R.id.tv_debug);
        iconIv = (ImageView)findViewById(R.id.icon_iv);

        lessonDb = new LessonKb(this);

        manager = ManagerFactory.getFactoryInstance().
                getManager(ManagerFactory.GENERAL_MANAGER, this, interestingBeacons);

        //business logic is attached here in the listner of beacon events
        posListener = new AbstractPositioningListener() {

            @Override
            public void onPositionChanged(IBeaconPositioningEvent iBeaconPositioningEvent) {

                //DEBUG
                debugTv.setText(iBeaconPositioningEvent.getDefaultRep());

                ArrayList<String> immBeacons = new ArrayList<>();
                ArrayList<String> nearBeacons = new ArrayList<>();
                ArrayList<String> farBeacons = new ArrayList<>();
                //calculate number of close beacons
                for(Map.Entry<String, IBeaconMeasure> entry : iBeaconPositioningEvent.getMeasures().entrySet()){
                    Utils.Proximity p = entry.getValue().getProximity();
                    if(p == Utils.Proximity.IMMEDIATE){
                        immBeacons.add(entry.getValue().getBeaconId());
                    } else if(p == Utils.Proximity.NEAR){
                        nearBeacons.add(entry.getValue().getBeaconId());
                    }else if(p == Utils.Proximity.FAR){
                        farBeacons.add(entry.getValue().getBeaconId());
                    }else {
                        //nothing default
                    }
                }
                //switch to state -> aggregate behaviour
                int local = nearBeacons.size() + immBeacons.size();
                if( local >= 2){
                    //if n>=2 beacons are at least near you are inside a room!
                        state = LessonState.INSIDE_LESSON;
                        String beaconId;
                        if(nearBeacons.size() > 0){
                            beaconId = nearBeacons.get(0);
                        } else {
                            beaconId = immBeacons.get(0);
                        }
                        insideLesson(beaconId);
                }else if(local == 1){
                    //close to 1 beacons -> close to a room!
                    if(state != LessonState.CLOSE_LESSON){
                        state = LessonState.CLOSE_LESSON;
                        String beaconId;
                        if(nearBeacons.size() > 0){
                            beaconId = nearBeacons.get(0);
                        } else {
                            beaconId = immBeacons.get(0);
                        }
                        closeLesson(beaconId);
                    }
                } else {
                    //no beacons near -> no room near
                    if(state != LessonState.FAR_LESSON){
                        state = LessonState.FAR_LESSON;
                        farLesson();
                    }
                }
            }
        };
        manager.addListener(posListener);

        Log.d(UtilsKB.LIB_TAG, "onCreate finished");
    }

    /**
     * State method for the "inside a room" state: when inside a room, the app try
     * to download the material for the lesson
     * @param beaconId
     */
    private void insideLesson(String beaconId){
        final String lesson = lessonDb.getLesson(beaconId);
        if(lesson != null) {

            if(!downloaded){
                Log.d(UtilsKB.LIB_TAG, "Trovata lezione Imm per " + beaconId);
                infoTv.setText("Stai seguendo: " + lesson);
                stateTv.setText(getResources().getString(R.string.immediate_state));
                iconIv.setImageResource(R.drawable.icon_notes);

                if(isFlat(getAccValues())){//flat
                    //chiedi se vuoi scaricare le slide più mostra la lezione attuale
                    downloaded = true;
                    showDownloadDialog(lesson);
                } else {//not flat
                    debugTv.setText("PHONE IS NOT FLAT");
                    Log.d(UtilsKB.LIB_TAG, "phone is not flat!");
                }
            } else {
                Log.d(UtilsKB.LIB_TAG, "already downloaded");
            }
        }
    }

    /**
     * State method for the "close to a room" state: when close to a room, the app advise the user
     * to go inside and attend the lesson
     * @param beaconId
     */
    private void closeLesson(final String beaconId){
        //mostra la lezione attuale + avvisa di avvicinarti alla cattedra
        downloaded = false;
        if(lessonDb.getLesson(beaconId) != null){
            Log.d(UtilsKB.LIB_TAG,"Trovata lezione near per "+beaconId);
            infoTv.setText("Sei vicino all'aula " + lessonDb.getRoom(beaconId));
            stateTv.setText(getResources().getString(R.string.near_state));
            iconIv.setImageResource(R.drawable.icon_lesson);
        }
    }

    /**
     * State method for the "far to each room" state: when far wrt every room, the app advise the
     * user to search for a lesson and go attending it.
     */
    private void farLesson(){
        Log.d(UtilsKB.LIB_TAG,"Far per .. ");
        downloaded = false;
        infoTv.setText(getResources().getString(R.string.no_room));
        stateTv.setText(getResources().getString(R.string.far_state));
        iconIv.setImageResource(R.drawable.unibo_logo2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.removeListener(posListener);
        Log.d(UtilsKB.LIB_TAG, "onDestroy finished");
    }

    /**
     * Show download dialog utility method: dialog to let the user decide if he wants to download material or not
     * @param lesson
     */
    private void showDownloadDialog(final String lesson){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.download_message)
                .setTitle(R.string.dialog_title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                //download slides
                String url = lesson.equals(getResources().getString(R.string.sir)) ? sir_pdf_url : issac_pdf_url;
                new DownloadTask().execute(url, lesson+".pdf");
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                //user pressed no!
                stateTv.append("*NO DOWNLOAD*");
                downloaded = false;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        Log.d(UtilsKB.LIB_TAG, "Dialog CREATA!");
    }

    /**
     * Download task to retrieve slides
     */
    private class DownloadTask extends AsyncTask<String,Void,Void>{

        private static final int  MEGABYTE = 1024;// * 1024;

        @Override
        protected Void doInBackground(String... strings) {

            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "Pervasivelesson-mat");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                if (!urlConnection.getContentType().equalsIgnoreCase("application/pdf")) {
                    Log.d("DEMO-LESSON","FAILED.\n[Sorry. This is not a PDF.]");
                    Log.d("DEMO-LESSON","type is "+urlConnection.getContentType());
                } else {
                    Log.d("DEMO-LESSON","YESS.\n[This is a PDF.]");
                }

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);
                int totalSize = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while((bufferLength = inputStream.read(buffer))>0 ){
                    fileOutputStream.write(buffer, 0, bufferLength);
                }
                fileOutputStream.flush();
                fileOutputStream.close();
                inputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Download Completed! :)",
                    Toast.LENGTH_LONG).show();
            downloaded = true;
        }
    }

    /**
     * Method to save accelerometer values.
     * @param event
     */
    @Override
    public synchronized void onSensorChanged(SensorEvent event) {
        accValues[0] = event.values[0];
        accValues[1] = event.values[1];
        accValues[2] = event.values[2];
    }

    /**
     * Retrieve accelerometer values.
     * @return
     */
    private synchronized float[] getAccValues(){
        return accValues;
    }

    /**
     * Utility to determine, given the vector value result of the accelerometer sensing, if the phone
     * is laying on a desk (flat, parallel to earth)
     * @param g
     * @return
     */
    private boolean isFlat(float[] g){
        double norm_Of_g = Math.sqrt(g[0] * g[0] + g[1] * g[1] + g[2] * g[2]);

        // Normalize the accelerometer vector
        double g0 = g[0] / norm_Of_g;
        double g1 = g[1] / norm_Of_g;
        double g2 = g[2] / norm_Of_g;

        int inclination = (int) Math.round(Math.toDegrees(Math.acos(g2)));
        if (inclination < 25 || inclination > 155) {
            // device is flat
            return true;
        } else {
            //int rotation = (int) Math.round(Math.toDegrees(Math.atan2(g[0], g[1])));
            return false;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
