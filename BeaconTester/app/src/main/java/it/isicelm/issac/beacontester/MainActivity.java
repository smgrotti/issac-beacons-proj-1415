package it.isicelm.issac.beacontester;

import android.os.AsyncTask;
import android.os.Environment;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Class to perform the tests with beacons: allows to set the type of test and store in csv file
 * the values of distance taken relative to a specific beacon
 */
public class MainActivity extends AppCompatActivity {
    public String APP_TAG = "BEACON-TESTER";
    private Map<String, ArrayList<BeaconMeasure>> measureMap = new HashMap<String, ArrayList<BeaconMeasure>>();
    private ArrayList<BeaconMeasure> measureList = new ArrayList<BeaconMeasure>();
    private BeaconManager beaconManager;
    private Region region;
    protected CSVWriter writer;
    private int buttonCheckedId;
    private int numIteration;
    private int count;

    /**
     * This method stores all the distance values notified by the SDK Estimote in special data structures
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonCheckedId = R.id.test_type_1m;
        numIteration = 0;
        count = 0;

        Log.d(APP_TAG, "IS EXT MEDIA READABLE " + isExternalStorageReadable());
        Log.d(APP_TAG, "IS EXT MEDIA WRITABLE " + isExternalStorageWritable());

        beaconManager = new BeaconManager(this);
        //put here the code to execute when a beacon inside the region is found
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> list) {
                count++;
                if (!list.isEmpty()) {
                    numIteration++;
                    //NB HERE WE ARE ON UI THREAD
                    //FOR HUGE COMPUTATION USE ASYNC
                    Log.d("BEACON_TEST", "-----------------------");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        ((TextView) findViewById(R.id.textMeasures)).append(
                            "------------------------\n" +
                                "Cicle #" +
                                numIteration+ "----" +count+
                                "Beacons found #" +
                                list.size() + "\n"
                        );
                        }
                    });
                    Log.d("BEACON_TEST", "Beacons found in this cicle #" + list.size());
                    for (Beacon b : list) {
                        if (b.getMajor() == BTUtils.MAJOR && b.getMinor() == BTUtils.MINOR) { //for select specific beacon
                            final BeaconMeasure measure = new BeaconMeasure(b.getProximityUUID(), b.getMajor(), b.getMinor(), b.getRssi(), b.getMeasuredPower(), Utils.computeAccuracy(b), Utils.computeProximity(b));
                            if (measureMap.get(b.getMajor() + ":" + b.getMinor()) == null) {
                                ArrayList<BeaconMeasure> t_measureList = new ArrayList<BeaconMeasure>();
                                t_measureList.add(measure);
                                measureMap.put(b.getMajor() + ":" + b.getMinor(), t_measureList);
                            } else measureMap.get(b.getMajor() + ":" + b.getMinor()).add(measure);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((TextView) findViewById(R.id.textMeasures)).append(measure.toString());
                                }
                            });
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            ((TextView) findViewById(R.id.textMeasures)).append(
                                    "------------------------\n"
                            );
                            }
                        });
                        Log.d("BEACON_TEST", "-----------------------");
                    }
                }
            }
        });

        region = new Region("ranged region", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);
        //connect the manager
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
            findViewById(R.id.start_button).setEnabled(true);
            TextView txt = (TextView) findViewById(R.id.tv_res);
            txt.setText(R.string.test_ready);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        beaconManager.stopRanging(region);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        beaconManager.disconnect();
        try {
            if(writer != null){
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(APP_TAG, "Error destroying csv writer");
        }
        super.onDestroy();
    }

    /**
     * Method called when the user touches the start button: take all the informations that are
     * set in the user interface in relation to the test to be performed
     */
    public void startTest(View view) {
        GridLayout gridLayout = ((GridLayout)findViewById(R.id.test_radio));
        RadioButton radioButton = (RadioButton)gridLayout.findViewById(buttonCheckedId);
        String meters = radioButton.getText().toString();

        CheckBox checkWall = (CheckBox)findViewById(R.id.checkbox_wall);
        String wall = checkWall.isChecked() ? "wall" : "";

        CheckBox checkPerson = (CheckBox)findViewById(R.id.checkbox_person);
        String person = checkPerson.isChecked() ? "person" : "";

        CheckBox checkMoving = (CheckBox)findViewById(R.id.checkbox_moving);
        String moving = checkMoving.isChecked() ? "moving" : "";

        RadioGroup radioGr = ((RadioGroup)findViewById(R.id.test_type));
        int checkedRadio = radioGr.getCheckedRadioButtonId();
        RadioButton radioButtonType = (RadioButton)radioGr.findViewById(checkedRadio);
        String type = radioButtonType.getText().toString();

        new TestTask().execute(meters, wall, person, moving, type);
    }

    /**
     * Class that represents an AsyncTask: we wanted to delegate to another control flow
     * management time duration of the test and the operations of input and output relating to csv file
     */
    private class TestTask extends AsyncTask<String, Void, String>{

        /**
         * In this method, we have:
         * .csv file creation
         * .management of the duration of the test
         * .save on a csv file all distance values stored
         */
        @Override
        protected String doInBackground(String... params) {
            boolean stop = false;
            File root = android.os.Environment.getExternalStorageDirectory();
            String param1 = params[1] != "" ? "_"+params[1] : "";
            String param2 = params[2] != "" ? "_"+params[2] : "";
            String param3 = params[3] != "" ? "_"+params[3] : "";
            String param4 = params[4] != "" ? "_"+params[4] : "";
            String nameFile = "TEST_"+params[0]+ param1 + param2 + param3 + param4 + ".csv";
            File file = new File(root.getAbsolutePath(), nameFile);

            //CSV FILE CREATION
            try {
                //writer = new CSVWriter(new FileWriter(file)); //CSVWriter not in append mode
                writer = new CSVWriter(new FileWriter(file,true));
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(APP_TAG, "Error creating csv writer");
            }

            beaconManager.startRanging(region);

            try {
                //WAIT FOR THE TEST DURATION
                if(params[4].equals("30detections")){
                    do{
                        Thread.sleep(500);
                        for(Map.Entry<String, ArrayList<BeaconMeasure>>  m : measureMap.entrySet()){
                            if(!(stop = (m.getValue().size() >= BTUtils.ITERATION_TEST))) break;
                        }
                    } while (!stop);
                }else{
                    Thread.sleep(BTUtils.DURATION_TEST * 1000);
                }
                
                beaconManager.stopRanging(region);

                //SAVE DISTANCE VALUES STORED TO CSV FILE
                for(Map.Entry<String, ArrayList<BeaconMeasure>>  m : measureMap.entrySet()){
                    ArrayList<BeaconMeasure> list = m.getValue();
                    String[] row = new String[list.size()];
                    for(int i = 0; i<list.size(); i++) {
                        row[i] = (""+(list.get(i).getBeaconDistance())).replace('.', ',');
                    }
                    writer.writeNext(row);
                }

                measureList = new ArrayList<BeaconMeasure>();
                measureMap = new HashMap<String, ArrayList<BeaconMeasure>>();

                if(writer != null){
                    writer.close();
                    writer = null;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                return getString(R.string.test_fail);
            } catch (IOException ex) {
                ex.printStackTrace();
                Log.e(APP_TAG, "Error destroying csv writer");
            }

            return getString(R.string.test_success);
        }

        @Override
        protected void onPostExecute(String result) {
            TextView txt = (TextView) findViewById(R.id.tv_res);
            txt.setText(result);
            findViewById(R.id.start_button).setEnabled(true);
            //reset values tv
            ((TextView) findViewById(R.id.textMeasures)).setText("Beacons Measurement Values");
        }

        @Override
        protected void onPreExecute() {
            TextView txt = (TextView) findViewById(R.id.tv_res);
            txt.setText(R.string.test_in_progress);
            findViewById(R.id.start_button).setEnabled(false);
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    /**
     * Management of radio buttons
     */
    public void onRadioButtonClicked(View view) {
        GridLayout gridLayout = ((GridLayout)findViewById(R.id.test_radio));
        ((RadioButton)gridLayout.findViewById(buttonCheckedId)).setChecked(false);
        buttonCheckedId = ((RadioButton) view).getId();
    }

    /**
     * Checks if external storage is available for read and write
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if external storage is available to at least read
     */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}