package it.isicelm.issac.beacontester;

import com.estimote.sdk.Utils;

import java.util.UUID;

/**
 * Support class that contains all information about a specific beacon
 */
public class BeaconMeasure{

    private UUID beaconUUID;
    private int beaconMajor;
    private int beaconMinor;
    private int beaconRSSI;
    private int beaconPower;
    private double beaconDistance;
    private Utils.Proximity beaconProximity;

    public BeaconMeasure(UUID beaconUUID, int beaconMajor, int beaconMinor, int beaconRSSI, int beaconPower, double beaconDistance, Utils.Proximity beaconProximity) {
        this.beaconUUID = beaconUUID;
        this.beaconMajor = beaconMajor;
        this.beaconMinor = beaconMinor;
        this.beaconRSSI = beaconRSSI;
        this.beaconPower = beaconPower;
        this.beaconDistance=beaconDistance;
        this.beaconProximity = beaconProximity;
    }

    public UUID getBeaconUUID() {
        return beaconUUID;
    }

    public void setBeaconUUID(UUID beaconUUID) {
        this.beaconUUID = beaconUUID;
    }

    public int getBeaconMajor() {
        return beaconMajor;
    }

    public void setBeaconMajor(int beaconMajor) {
        this.beaconMajor = beaconMajor;
    }

    public int getBeaconMinor() {
        return beaconMinor;
    }

    public void setBeaconMinor(int beaconMinor) {
        this.beaconMinor = beaconMinor;
    }

    public int getBeaconRSSI() {
        return beaconRSSI;
    }

    public void setBeaconRSSI(int beaconRSSI) {
        this.beaconRSSI = beaconRSSI;
    }

    public int getBeaconPower() {
        return beaconPower;
    }

    public void setBeaconPower(int beaconPower) {
        this.beaconPower = beaconPower;
    }

    public double getBeaconDistance() {
        return beaconDistance;
    }

    public void setBeaconDistance(double beaconDistance) {
        this.beaconDistance = beaconDistance;
    }

    public Utils.Proximity getBeaconProximity() {
        return beaconProximity;
    }

    public void setBeaconProximity(Utils.Proximity beaconProximity) {
        this.beaconProximity = beaconProximity;
    }

    @Override
    public String toString() {
        return "Beacon->"+beaconMajor+":"+beaconMinor+"--"+
                "MP=> "+beaconPower+"--"+
                "RSSI=> "+beaconRSSI+"--"+
                "D=> "+beaconDistance+"--"+
                "Proximity => "+beaconProximity.toString()+"\n";
    }
}