package it.isicelm.issac.beacontester;

/**
 * Utility class that contains information on the duration of the test,
 * the beacon identifier to be monitored and finally a method for calculating the distance
 */
public class BTUtils {

    public static int MAJOR = 58177;
    public static int MINOR =52282;
    public static int DURATION_TEST = 300;
    public static int ITERATION_TEST = 31;

    /**
     * Calculates the distance given the PowerMeasure and RSSI. Calibrated on Nexus 4.
     * Taken from http://developer.radiusnetworks.com/2014/12/04/fundamentals-of-beacon-ranging.html
     */
    public static double calculateDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine distance, return -1.
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return accuracy;
        }
    }
}
